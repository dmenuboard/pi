# README #

DMENUBOARD PI Application Latest Version Repository 

### This Repository is the installer for the DmenuBoard PI Application ###

* Available Now in Beta Version
* Software Copyrigths DmenuBoard 2018
* [Visit Our Site for more information and Sign up For Free](http://cloud.dmenuboard.com)

### How do I get set up? ###

* Get a PI 3B+ from Amazon or any vendor you like, make sure is a PI 3B+
* Prepare a 16GB or better 32GB SD Card with the Raspbian RASPBIAN STRETCH LITE
 >[Comunity instruction](https://www.raspberrypi.org/downloads/raspbian/)
 >Choose RASPBIAN STRETCH LITE.
* Flash a new 32GB SD Card using a free tool like [Etcher](https://etcher.io) 

### Once you have the Raspberry PI 3 B+ with the SD Card Flashed. ###

* Connect the PI to a TV screen, mouse and keyboard
* Insert the SD CARD in the PI
* Power up the PI
* Open a Terminal
* type in the following command and press enter:

	>git clone https://dmenuboard@bitbucket.org/dmenuboard/pi.git

* once the download show completed, type the following:

	>cd pi

* Type go and press enter/return to start the installer 

	>sh go

* The DMANU Installer will do the rest.

### Contribution guidelines ###

* Author Giorgio Francescangeli
* DmenuBoard Co-Founder
