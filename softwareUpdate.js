/******************************************************
* Digital Menu Boards Copyrigths 2018
* 
* To install the application run the following command
*    $ sudo node install.js
*
******************************************************/


const cmd = require('child_process').execSync;
const path = require('path');
const fs = require('fs');
var opt = {encoding:'utf8'};

console.log(cmd('pwd',opt));
//Basic Command to update the application
console.log('Software Update Application');
var output = cmd(' unzip -o dmenuPI.zip -d ~/DMENU',opt);
console.log(output);
console.log('Done!');

console.log('Make startdmenu.sh file executable');
cmd('chmod 775 startdmenu.sh');
console.log('Done!');

console.log('3/12 Copy startdmenu.sh file to DMENU/');
cmd('cp startdmenu.sh ~/DMENU/');
console.log('Done!');

// cmd('rm ./swupdate.flag');

//cmd('');
cmd('{ nohup ~/DMENU/startdmenu.sh & } &');

return 0;