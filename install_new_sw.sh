#!/bin/bash


pcmanfm --set-wallpaper ~/DMENU/NEWSW/pi/softwareUpdate.png
# Update Software as first thing
unzip -o ~/DMENU/NEWSW/pi/dmenuPI.zip -d ~/DMENU

# Clean the current pi repo
rm -rfv ~/DMENU/pi

# copy the newsw as current repo
cp -fR ~/DMENU/NEWSW/pi ~/DMENU/pi

# Copy the new startdmenu.sh into the application dir
cp ~/DMENU/NEWSW/pi/startdmenu.sh ~/DMENU/startdmenu.sh
# Ensure is executable
sudo chmod 755 ~/DMENU/startdmenu.sh

#Once done, create the flag for new sw ready
#touch ~/DMENU/NEWSW/new.sw.ready
touch ~/DMENU/resources/data/new.sw.ready

pcmanfm --set-wallpaper ~/DMENU/pi/dmenuboard-bg.jpg

reboot
